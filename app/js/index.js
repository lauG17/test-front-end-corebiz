
/*----------------------------------- carrusel banner*/
$(document).ready(function(){
    $('.banner .owl-carousel').owlCarousel({
        items:1
    });
    updateCart();
  });

  /*-------------------------------------- productos*/
  fetch('https://corebiz-test.herokuapp.com/api/v1/products')
  .then(function(response) {
      return response.json();
  })
  .then(data => getProduct(data))
  .catch(function(err) {
      console.log(err);
  });
  
function getProduct (data) {
    data.forEach(element => {
        console.log(element)
        createCarusel(element);      
    });
}

function createTemplate(HTMLString) {
    const html = document.implementation.createHTMLDocument();
    html.body.innerHTML = HTMLString;
    return html.body.children[0];
}

function createTemplateString(data) {
    var stars = data.stars ? data.stars : 0;
    var installments = data.installments.length !== 0  ? data.installments[0] : null;
    console.log("install",installments);
    var installmentQuantity = installments ? data.installments[0].quantity : null;
    var installmentValue = installments ? data.installments[0].value : null;
    var lastPrice = data.listPrice ? data.listPrice : null;

    return (
      `<li >
         <img class = "image-product" src = "${data.imageUrl}">
         <img class ="${lastPrice ? "last-price":"not-last-price"}" src = "./Image/Flag.svg" >
         <h2>${data.productName}</h2>
         <img src="./Image/Rating-stars-${data.stars}.svg">
         <h3 class = "${lastPrice ? "last-price":"not-last-price"}">de R$ ${lastPrice}</h3>
         <h1>por R$ ${data.price}</h1>
         <p class="${installments ? "installment":"not-installment"}"> ou em ${installmentQuantity }x de R$ ${installmentValue}</p>
         <button type="button" onclick = "buy()" >Comprar</button>
       </li>`
    )
}


 /*----------------------------- carrusel productos */
function createCarusel (data) {
    $container = document.querySelector(".carousel-products");
    var HTMLString = createTemplateString(data);
    var element = createTemplate(HTMLString);
    $container.append(element);

    setTimeout(() => {
        $('.carousel-products').owlCarousel({
            margin:1,
            loop:true,
            responsiveClass:true,
            navContainer: '#navhere',
    responsive:{
        300:{
            items:2,
            nav:false,
            loop: true,
        },
        1000:{
            items:4,
            nav:true,
            loop:true,

        }
    }
});
    }, 1000);

}

/*------------------------------------------Compra de producto*/
function buy() {
    var cartProduct = localStorage.getItem('cartProduct');
    console.log(cartProduct)
    cartProduct = cartProduct ? ++cartProduct : 1
    localStorage.setItem('cartProduct', cartProduct);
    updateCart();
}

function updateCart () {
    var cart = document.getElementsByClassName("cont-cart")[0] 
    var cartLocal = localStorage.getItem('cartProduct')
    cart.innerHTML = cartLocal ? cartLocal : 0 
    var mobileCart = document.getElementsByClassName("mobile-cont-cart")[0] 
    mobileCart.innerHTML = cartLocal ? cartLocal : 0 
    if ((cart.innerHTML == 0) && (mobileCart.innerHTML == 0)){
        document.querySelector(".cont-cart").style.display = "none";
        document.querySelector(".mobile-cont-cart").style.display = "none";
    }
    else {
        document.querySelector(".cont-cart").style.display = "block";
        document.querySelector(".mobile-cont-cart").style.display = "block";
    }
}

 /*-------------------------------Validacion formulario */
 function validateName (name) {
    return /^(?=(?![0-9])?[A-Za-z0-9]?[._-]?[A-Za-z0-9]+).{3,20}/.test(name);
}

 function validateMail (mail) {
    emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    return emailRegex.test(mail);
 }

 function validateForm(event) {
    event.preventDefault()
    var name = document.getElementById("name").value
    isValidName = validateName(name);
    var mail = document.getElementById("mail").value
    isValidMail = validateMail(mail);

    if (isValidName && isValidMail) {
        insertForm(mail,name);

    } 
    else {
        if (!isValidName) {
            document.querySelector(".message-name").style.display = "block";
            document.querySelector(".name").style.border = "1px solid #D7182A";
            document.querySelector(".button-form").style.background = "#333333";
            
        }
        if (!isValidMail) {
            document.querySelector(".message-email").style.display = "block";
            document.querySelector(".mail").style.border = "1px solid #D7182A";
            document.querySelector(".button-form").style.background = "#333333";
            
        }
    }
 }

 /*--------------------------------------------------Nuevo mail*/
 function newMail() {
    document.querySelector(".acept-form").style.display = "none";
    document.querySelector(".bulletin-form").style.display = "grid";
    document.querySelector(".bulletin-title").style.display = "block";
    document.getElementById("name").value = '';
    document.getElementById("mail").value = '';
 }

 /*------------------------------------------------ Boletin */
 function messageForm (response) {
    if (response.message == 'Created successfully') {
        document.querySelector(".bulletin-form").style.display = "none";
        document.querySelector(".bulletin-title").style.display = "none";
        document.querySelector(".acept-form").style.display = "block";
    }
 }
function insertForm (mail,name) {
    var url = 'https://corebiz-test.herokuapp.com/api/v1/newsletter';
    var data = {'email': mail, 'name': name};

    fetch(url, {
    method: 'POST', 
    body: JSON.stringify(data),
    headers:{
        'Content-Type': 'application/json'
    }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => messageForm( response));
}