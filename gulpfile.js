var gulp        = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var sass        = require('gulp-sass');
var plumber = require('gulp-plumber');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');

// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    //watch files
    var files = [
    './app/css/*.css',
    './app/js/*.js',
    './app/*.html'
    ];

    //initialize browsersync
    browserSync.init(files, {
        server: {
            baseDir: 'app'
          },
    });
});

// Sass task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task('sass', function () {
    return gulp.src('./app/sass/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest('./app/css'))
    .pipe(reload({stream:true}));
});

gulp.task('uglify', function() {
    gulp.src('./app/js/**/*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest('min_js'));
});

gulp.task('imagemin', function() {
    gulp.src('img/**/*.{jpg,jpeg,png,gif}')
    .pipe(plumber())
    .pipe(imagemin())
    .pipe(gulp.dest('img_optimize'));
});

// Default task to be run with `gulp`
gulp.task('default', ['uglify', 'imagemin', 'sass', 'browser-sync'], function () {
    gulp.watch("./app/sass/*.scss", ['sass']);
    gulp.watch('./app/js/**/*.js', ['uglify']);
    gulp.watch('img/**/*.{jpg,jpeg,png,gif}', ['imagemin']);
});
