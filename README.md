# Prueba técnica Corebiz

El desarrollo de esta prueba se hizo, maquetando en html y sass, el diseño enviado por medio de figma,
se utilizó javascript Vanilla y fetch para hacer peticiones a la api correspondiente


## Las siguientes instruccion corresponden a la compilacion y ejecucion del proyecto:

1. En la terminal despues de haber clonado el proyecto de gitlab, ejecutar el comando `npm install`
2. Despues de cargarse todas las dependencias del proyecto, ejecutar el comando `npm run gulp` el cual sera nuestro automatizador de tareas
> Al realizar esto, se desplegara automaticamente una ventana en el explorador predeterminado, por el sistema operativo, donde se 
visualizará la página

